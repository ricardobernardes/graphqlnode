const knex = require('../config/database')

module.exports = {
    Query:{
        // async getUser(_, { params }){
        //     return await knex('users').where({ id : params.id })
        // }
        async getUser(_, { id }){
            return await knex('users').where({ id }).first()
        },   
        
        async getUserEmail(_, { email }){
            return await knex('users').where({ email }).first()
        },           
        
        async getUsers(){
            return await knex('users')
        }             
    },

    Mutation:{
        async createUser(_, { input }){
            debugger;
            const result = await  knex('users').insert({
                name: input.name,
                email: input.email,
                password: input.password
            })
            .returning('id')

            const id = result[0]
            console.log(id)    
            return await knex('users').where({ id }).first()
        }      
    }
}